# "Reverse Engineering for Beginners" free ebook

Topics discussed: x86/x64, ARM/ARM64, MIPS, Java/JVM.

Compiled versions can be found here: https://beginners.re/

Want access to all RE4B PDFs Go [here](https://archive.org/details/ReverseEngineeringforBeginners) and for UAL PDFs go [here](https://archive.org/details/UnderstandingAssemblyLanguage).

# Contact
If you are Dennis Yurichev and want to take down this repository, then message me on Matrix (@sag:nitro.chat)